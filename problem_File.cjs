let p_File_Obj = require('./1-arrays-jobs.cjs');

//problem1

const is_W_Developer = (ob) => 
{
    if((ob.job).toLowerCase().includes(('web developer').toLowerCase()))
    {
        return true;
    }
    return false;
}

let web_Devs = p_File_Obj.filter(is_W_Developer);
//console.log(web_Devs);

// problem2

p_File_Obj.forEach((ob) => {
    ob.salary = +(ob.salary.substring(1));
});

//problem3

p_File_Obj.forEach((ob) => {
    ob.accurate_Salary = +((ob.salary)*10000);
});

//problem4

let salary_Sum = p_File_Obj.reduce(
    (accumulator,currentValue) => accumulator + currentValue.salary,0);
    // console.log(salary_Sum);

//problem5

let salary_Country = [];
p_File_Obj.forEach((ob) => {
    if(ob.location in salary_Country)
    {
        salary_Country[ob.location] += ob.salary; 
    }
    else
    {
        salary_Country[ob.location] = ob.salary;
    }
});
// console.log(salary_Country);

//problem6

let num_of_times_Country = [];
p_File_Obj.forEach((ob) => {
    if(ob.location in num_of_times_Country)
    {
        num_of_times_Country[ob.location] += 1; 
    }
    else
    {
        num_of_times_Country[ob.location]= 1; 
    }
});
console.log(num_of_times_Country);
